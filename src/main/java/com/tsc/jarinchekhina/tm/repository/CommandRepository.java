package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.constant.ArgumentConst;
import com.tsc.jarinchekhina.tm.constant.TerminalConst;
import com.tsc.jarinchekhina.tm.model.Command;

public class CommandRepository {

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "display developer info"
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "display program version"
    );

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "display list of commands"
    );

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "display system information"
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "close application"
    );

    public static final Command[] COMMANDS = new Command[] {
            ABOUT, VERSION, HELP, INFO, EXIT
    };

    public static Command[] getCommands() {
        return COMMANDS;
    }

}
