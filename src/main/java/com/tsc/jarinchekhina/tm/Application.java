package com.tsc.jarinchekhina.tm;

import com.tsc.jarinchekhina.tm.constant.ArgumentConst;
import com.tsc.jarinchekhina.tm.constant.TerminalConst;
import com.tsc.jarinchekhina.tm.model.Command;
import com.tsc.jarinchekhina.tm.repository.CommandRepository;
import com.tsc.jarinchekhina.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        System.out.println("** Welcome to Task Manager **");
        parseArgs(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public static void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    public static void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT: showAbout(); break;
            case ArgumentConst.VERSION: showVersion(); break;
            case ArgumentConst.HELP: showHelp(); break;
            case ArgumentConst.INFO: showInfo(); break;
            default: showErrorArgument();
        }
    }

    public static void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT: showAbout(); break;
            case TerminalConst.VERSION: showVersion(); break;
            case TerminalConst.HELP: showHelp(); break;
            case TerminalConst.INFO: showInfo(); break;
            case TerminalConst.EXIT: exit(); break;
            default: showErrorCommand();
        }
    }

    public static void exit() {
        System.exit(0);
    }

    private static void showErrorArgument() {
        System.err.println("Error! Argument not supported");
        System.exit(1);
    }

    private static void showErrorCommand() {
        System.err.println("Error! Command not found");
    }

    public static void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer: Yuliya Arinchekhina");
        System.out.println("E-mail: arin-akira@mail.ru");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.7.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        Command[] commands = CommandRepository.getCommands();
        for (final Command command: commands) System.out.println(command);
    }

}